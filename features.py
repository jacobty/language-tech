######## Dependency parsing

import conll
import dparser
import transition

def extract(stack, queue, graph, feature_names, sentence):
    """ Return the features in a dictionary format compatible with scikit learn
    """

    x = []
    
    if len(feature_names) == 6:
        for a in (stack, queue):
            if a:
                x.append(a[0]['postag'])
                x.append(a[0]['form'])
            else:
                x.append('nil')
                x.append('nil')
        x.append(transition.can_reduce(stack, graph))
        x.append(transition.can_leftarc(stack, graph))

    elif len(feature_names) > 6:
        form0 = 'nil'
        form1 = 'nil'
        for a in (stack, queue):
            if a:
                x.append(a[0]['postag'])
                form0 = a[0]['form']
                if len(a) > 1: 
                    x.append(a[1]['postag'])
                    form1 = a[1]['form']
                else:
                    x.append('nil')
            else: 
                x.append('nil')
                x.append('nil')
            x.append(form0)
            x.append(form1) 
        if len(feature_names) == 14:
            if stack:
                index = int(stack[0]['id'])
                if index > 0:
                    prev_word = sentence[index - 1]['form']
                    prev_pos = sentence[index - 1]['postag']
                    x.append(prev_word)
                    x.append(prev_pos)
                else:
                    x.append('nil')
                    x.append('nil')
                if index < len(sentence) - 1:
                    next_word = sentence[index + 1]['form']
                    next_pos = sentence[index + 1]['postag']
                    x.append(next_word)
                    x.append(next_pos)
                else: 
                    x.append('nil')
                    x.append('nil')

            else:
                x.append('nil')
                x.append('nil')
                x.append('nil')
                x.append('nil')
        x.append(transition.can_reduce(stack, graph))
        x.append(transition.can_leftarc(stack, graph))
    features = dict(zip(feature_names, x))

    return features


def extract_all(stack, queue, graph, feature_names, all_sentences):
    X = []
    for sentence in all_sentences:
        X.append(extract(stack, queue, graph, feature_names, sentence))
    return X

if __name__ == '__main__':
    start_time = time.clock()
    train_corpus = 'swedish_talbanken05_train.conll.txt'
    test_corpus = 'swedish_talbanken05_test_blind.conll.txt'
    column_names_2006 = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats', 'head', 'deprel', 'phead', 'pdeprel']
    column_names_2006_test = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats']
    sentences = conll.read_sentences(train_corpus)
    formatted_corpus = conll.split_rows(sentences, column_names_2006)
    feature_names_1 = ['stack0_POS', 'stack0_word', 'queue0_POS',
                        'queue0_word', 'can-re', 'can-la']

    feature_names_2 = ['stack0_POS', 'stack1_POS' 'stack0_word', 'stack1_word', 
                        'queue0_POS', 'queue1_POS', 'queue0_word', 'queue1_word', 
                        'can-re', 'can-la']

    feature_names_3 = ['stack0_POS', 'stack1_POS' 'stack0_word', 'stack1_word', 
                        'queue0_POS', 'queue1_POS', 'queue0_word', 'queue1_word',
                        'prev_word', 'prev_POS', 'next_word', 'next_POS',
                        'can-re', 'can-la']


    