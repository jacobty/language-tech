"""
Gold standard parser
"""
__author__ = "Pierre Nugues"

import sys
from time import perf_counter
import transition
import conll
import features
from sklearn.feature_extraction import DictVectorizer
from sklearn import linear_model
from sklearn import metrics
import pickle

def reference(stack, queue, graph):
    """
    Gold standard parsing
    Produces a sequence of transitions from a manually-annotated corpus:
    sh, re, ra.deprel, la.deprel
    :param stack: The stack
    :param queue: The input list
    :param graph: The set of relations already parsed
    :return: the transition and the grammatical function (deprel) in the
    form of transition.deprel
    """
    # Right arc
    if stack and stack[0]['id'] == queue[0]['head']:
        # print('ra', queue[0]['deprel'], stack[0]['cpostag'], queue[0]['cpostag'])
        deprel = '.' + queue[0]['deprel']
        stack, queue, graph = transition.right_arc(stack, queue, graph)
        return stack, queue, graph, 'ra' + deprel
    # Left arc
    if stack and queue[0]['id'] == stack[0]['head']:
        # print('la', stack[0]['deprel'], stack[0]['cpostag'], queue[0]['cpostag'])
        deprel = '.' + stack[0]['deprel']
        stack, queue, graph = transition.left_arc(stack, queue, graph)
        return stack, queue, graph, 'la' + deprel
    # Reduce
    if stack and transition.can_reduce(stack, graph):
        for word in stack:
            if (word['id'] == queue[0]['head'] or
                        word['head'] == queue[0]['id']):
                # print('re', stack[0]['cpostag'], queue[0]['cpostag'])
                stack, queue, graph = transition.reduce(stack, queue, graph)
                return stack, queue, graph, 're'
    # Shift
    # print('sh', [], queue[0]['cpostag'])
    stack, queue, graph = transition.shift(stack, queue, graph)
    return stack, queue, graph, 'sh'

def parse_ml(stack, queue, graph, trans):
    if stack and trans[:2] == 'ra':
        stack, queue, graph = transition.right_arc(stack, queue, graph, trans[3:])
        return stack, queue, graph, 'ra'

    elif stack and transition.can_leftarc(stack, graph) and trans[:2] == 'la':
        stack, queue, graph = transition.left_arc(stack, queue, graph, trans[3:])
        return stack, queue, graph, 'la'

    elif stack and transition.can_reduce(stack, graph) and trans[:2] == 're':
        stack, queue, graph = transition.reduce(stack, queue, graph)
        return stack, queue, graph, 're'

    else: 
        stack, queue, graph = transition.shift(stack, queue, graph)
        return stack, queue, graph, 'sh'

def predict_ml(classifier, vec, formatted_corpus, feature_names, mode, column_names):    
    for sentence in formatted_corpus:
        stack = []
        queue = list(sentence)
        graph = {}
        graph['heads'] = {}
        graph['heads']['0'] = '0'
        graph['deprels'] = {}
        graph['deprels']['0'] = 'ROOT'
        transitions = []

        while queue:
            feature_dict = features.extract(stack, queue, graph, feature_names, sentence)
            X = vec.transform(feature_dict)
            trans_predict = classifier.predict(X)[0]
            stack, queue, graph, trans = parse_ml(stack, queue, graph, trans_predict)
        stack, graph = transition.empty_stack(stack, graph)

        # Poorman's projectivization to have well-formed graphs.
        for word in sentence:
            word['head'] = graph['heads'][word['id']]
            word['deprel'] = graph['deprels'][word['id']]

    conll.save('out'+mode, formatted_corpus, column_names)

def feature_names(mode):    
    feature_names_0 = ['stack0_POS', 'stack0_word', 'queue0_POS', 'queue0_word', 'can-re', 'can-la']

    feature_names_1 = ['stack0_POS', 'stack1_POS', 'stack0_word', 'stack1_word', 'queue0_POS', 'queue1_POS', 'queue0_word', 'queue1_word', 'can-re', 'can-la']
    
    feature_names_2 = ['stack0_POS', 'stack1_POS', 'stack0_word', 'stack1_word', 'queue0_POS', 'queue1_POS', 'queue0_word', 'queue1_word', 'prev_word', 'prev_POS', 'next_word', 'next_POS', 'can-re', 'can-la']

    if mode == '0':
        feature_names = feature_names_0
    if mode == '1':
        feature_names = feature_names_1
    if mode == '2':
        feature_names = feature_names_2
    return feature_names

if __name__ == '__main__':
    start_time = perf_counter()
    mode = sys.argv[1]
    train_file = 'swedish_talbanken05_train.conll.txt'
    test_file = 'swedish_talbanken05_test_blind.conll.txt'
    column_names_2006 = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats', 'head', 'deprel', 'phead', 'pdeprel']
    column_names_2006_test = ['id', 'form', 'lemma', 'cpostag', 'postag', 'feats']

    sentences = conll.read_sentences(train_file)
    formatted_corpus = conll.split_rows(sentences, column_names_2006)

    feature_names = feature_names(mode)
    print(feature_names)
    print(len(feature_names))
    print("Extracting the features...")

    feature_list = []
    Y = []
    sent_cnt = 0
    shortest_sentence = formatted_corpus[0]
    length = 10000000
    sent_np_cnt = 0    
    for sentence in formatted_corpus:
        sent_cnt += 1
        if sent_cnt % 1000 == 0:
            print(sent_cnt, 'sentences on', len(formatted_corpus), flush=True)
        stack = []
        queue = list(sentence)
        graph = {}
        graph['heads'] = {}
        graph['heads']['0'] = '0'
        graph['deprels'] = {}
        graph['deprels']['0'] = 'ROOT'
        transitions = []

        while queue:
            feature_list.append(features.extract(stack, queue, graph, feature_names, sentence))
            stack, queue, graph, trans = reference(stack, queue, graph)
            transitions.append(trans)
        stack, graph = transition.empty_stack(stack, graph)
        #print('Equal graphs:', transition.equal_graphs(sentence, graph))

        a = transition.equal_graphs(sentence,graph)
        if a:
            sent_np_cnt += 1
        if a and len(a) < length:
            length = len(a)
            shortest_sentence = a


        # Poorman's projectivization to have well-formed graphs.
        for word in sentence:
            word['head'] = graph['heads'][word['id']]
        Y.extend(transitions)


    print('Shortest sentence that is non-projective is: ')
    print(shortest_sentence)
    print('Amount of non-projective sentences:')
    print(sent_np_cnt)

    try:
        with open('model'+mode, 'rb') as m:
            model = pickle.load(m)
        with open('vec'+mode, 'rb') as v:
            vec = pickle.load(v)

    except FileNotFoundError:
        print('Encoding the features...')
        vec = DictVectorizer(sparse=True)
        X_train = vec.fit_transform(feature_list)
        classifier = linear_model.LogisticRegression(penalty='l2', dual=False, solver='lbfgs', max_iter=100, multi_class='auto')
        print("Training the model...")
        model = classifier.fit(X_train, Y)
        print(model)
        with open('model' + mode, 'wb') as m:
            pickle.dump(model, m)
        with open('vec' + mode, 'wb') as v:
            pickle.dump(vec, v)

    ##Apply model to train set
    test_start_time = perf_counter()
    train_time = str(test_start_time - start_time)
    print('Training time: ' + train_time + ' minutes')

    #X = vec.transform(feature_list)
    #y_train_predicted = model.predict(X)
    #print("Classification report for classifier %s:\n%s\n"
    #        % (model, metrics.classification_report(Y, y_train_predicted)))

    ##Assignment 6
    
    test_sentences = conll.read_sentences(test_file)
    formatted_test_corpus = conll.split_rows(test_sentences, column_names_2006_test)

    print('Predicting...')
    predict_ml(model, vec, formatted_test_corpus, feature_names, mode, column_names_2006)

    end_time = perf_counter()
    time = str((end_time - test_start_time) / 60)
    print('Test time: ' + time + ' minutes')
